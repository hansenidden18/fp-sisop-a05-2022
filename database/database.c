#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <stdbool.h>
#include <errno.h>
#include <sys/stat.h>
#include <pthread.h>
#include <limits.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#define PORT 8080

int curr_fd = -1, curr_ids = -1;
const int SIZE_BUFFER = sizeof(char) * 1000;

char workDir[PATH_MAX];

bool login(int fd, char *username, char *password){
    int ids = -1;
    if(curr_fd != -1){
        write(fd, "Server busy\n", SIZE_BUFFER);
        return false;
    }

    if(strcmp(username, "root") == 0) ids = 0;

    if(ids != -1){
        write(fd, "Login success\n", SIZE_BUFFER);
        curr_fd = fd;
        curr_ids = ids;
    }
    return true;
}

bool checkFile(char *filename){
    struct stat buffer;
    return (stat(filename, &buffer) == 0);
}

void logBos(char *str){
    FILE *fp = fopen("/home/awan/data.log", "a+");

    time_t waktu;
    struct tm *info;
    char buffer[1000];
    
    time(&waktu);

    info = localtime(&waktu);

    strftime(buffer, sizeof(buffer), "%Y-%m-%d %X", info);
    fprintf(fp, "%s:root:%s\n", buffer, str);
    fclose(fp);
}

void *routes(void *argv){
    int fd = *(int *)argv;
    char query[1005], buffer[1005];
    char path[1005];

    while(read(fd, query, 1000) != 0){
        puts(query);
        strcpy(buffer, query);
        logBos(buffer);
        char *command = strtok(buffer, " ");

        if(strcmp(command, "LOGIN") == 0){
            char *username = strtok(NULL," ");
            char *password = "root";
            if (!login(fd, username, password))break;
        }
        else if(strcmp(command, "CREATE") == 0){
            command = strtok(NULL," ");

            if(strcmp(command, "DATABASE") == 0){
                char dirRoot[10000], dirDB[10000], cwd[PATH_MAX], tmp[PATH_MAX];
                char *token = strtok(NULL, " ");

                getcwd(cwd, sizeof(cwd));
                strcpy(tmp, cwd);

                printf("Temporary Directory: %s\n", tmp);

                if(strstr(cwd, "all_database/") != NULL) chdir("../../");

                if(getcwd(cwd,sizeof(cwd)) != NULL){
                    char *namaDB = strtok(token,";");

                    sprintf(dirDB, "%s/all_database/%s", cwd, namaDB);
                    sprintf(dirRoot, "%s/all_database",cwd);
                    
                    DIR *dir = opendir(dirRoot);
                    if(ENOENT == errno) mkdir(dirRoot, 0777);

                    if(!mkdir(dirDB, 0777)){
                        write(fd, "Database created successfully\n", SIZE_BUFFER);

                        chdir(tmp);
                    } else{
                        write(fd, "Database failed to create\n", SIZE_BUFFER);
                    }
                }
            }
            else if(strcmp(command, "USER") == 0){
                char fileDir[10000], cwd[PATH_MAX], tmp[PATH_MAX];
                char *nama_user = strtok(NULL, " ");
                strtok(NULL, " ");
                strtok(NULL, " ");

                getcwd(cwd, sizeof(cwd));
                strcpy(tmp, cwd);

                if(strstr(cwd, "all_database/") != NULL) chdir("../../");

                if(getcwd(cwd, sizeof(cwd)) != NULL){
                    FILE *data;
                    char *pass_baru = strtok(NULL,";");

                    sprintf(fileDir, "%s/%s.txt", cwd, "user");

                    data = fopen(fileDir, "a");

					fprintf(data, "%s %s\n", nama_user, pass_baru);
					fclose(data);
					write(fd, "User berhasil dibuat!\n", SIZE_BUFFER);
                }
            }
            else if(strcmp(command, "TABLE") == 0){
                char fileDir[10000], cwd[PATH_MAX];
                char *nama_tabel = strtok(NULL, " ");

                printf("Table: %s\n", nama_tabel);

                if(getcwd(cwd, sizeof(cwd)) != NULL){
                    FILE *data;
                    char *kolom = strtok(NULL,"()");

                    printf("Column: %s\n", kolom);
                    sprintf(fileDir, "%s/%s.txt", cwd, nama_tabel);

                    printf("Directory: %s\n", fileDir);

                    if (strstr(cwd, "all_database/") == NULL){
						write(fd, "Database not used!\n", SIZE_BUFFER);
				    }
					else if (checkFile(fileDir)){
						write(fd, "Table already exist!\n", SIZE_BUFFER);
					}
					else if (kolom == NULL){
						write(fd, "Invalid syntax!\n", SIZE_BUFFER);
					}
					else{
						data = fopen(fileDir, "a");

						fprintf(data, "%s\n", kolom);
						fclose(data);
						write(fd, "Tabel berhasil dibuat!\n", SIZE_BUFFER);
					}
				}
			}
			else{
				write(fd, "Invalid query on CREATE command\n", SIZE_BUFFER);
			}
        }
        else if(strcmp(command, "USE") == 0){
            char cwd[PATH_MAX], dirDB[10000];
            command = strtok(NULL, " ");

            getcwd(cwd, sizeof(cwd));

            if(strstr(cwd, "all_database/") != NULL) chdir("../../");

            if(getcwd(cwd, sizeof(cwd)) != NULL){
                char tmp[10000];
                char *namaDB = strtok(command, ";");

                printf("%s\n", namaDB);
                printf("cwd: %s\nCommand: %s\n", cwd, command);
                sprintf(dirDB, "%s/all_database/%s", cwd, command);
                printf("Direcotory: %s\n", dirDB);

                if(chdir(dirDB) == 0){
                    write(fd, "Move Database Successfully\n", SIZE_BUFFER);
                } else{
                    write(fd, "Failed to move Database\n", SIZE_BUFFER);
                }
            }
        }
        else if(strcmp(command, "DROP") == 0){
            command = strtok(NULL," ");

            if(strcmp(command, "DATABASE") == 0){
                char string[10000], cwd[PATH_MAX], tmp[PATH_MAX];
                char *token = strtok(NULL, " ");

                getcwd(cwd, sizeof(cwd));
                strcpy(tmp, cwd);

                printf("Temp: %s\n", tmp);

                if(strstr(cwd, "all_database/") != NULL) chdir("../../");

                if(getcwd(cwd, sizeof(cwd)) != NULL){
                    
                    DIR *dp;
                    struct dirent *ep;
                    char path[1000], fileDir[10500];
                    char *token1 = strtok(token, ";");

                    sprintf(string, "%s/all_database/%s", cwd, token1);

                    dp = opendir(string);
                    if(dp != NULL){
                        while((ep = readdir(dp))){
                            if(ep->d_type == DT_REG){
                                puts(ep->d_name);
                                sprintf(fileDir, "%s/%s", string, ep->d_name);
                                remove(fileDir);
                            }
                        }
                        (void) closedir(dp);
                    }
                    if(!rmdir(string)){

                        write(fd, "Database dropped successfuly\n", SIZE_BUFFER);

                        chdir(tmp);
                    } else{
                        write(fd, "Database failed to be drop\n", SIZE_BUFFER);
                    }
                }
            }
            else if(strcmp(command, "TABLE") == 0){
                char string[10000], cwd[PATH_MAX];
                char *token = strtok(NULL, " ");

                if(getcwd(cwd, sizeof(cwd)) != NULL){
                    char *token1 = strtok(token, ";");
                    sprintf(string, "%s/%s", cwd, token1);

                    if(strstr(cwd, "all_database/") == NULL){
                        write(fd, "Database not use!\n", SIZE_BUFFER);
                    } else{
                        if(!remove(string)){
                            write(fd, "Table removed successfuly\n", SIZE_BUFFER);
                        } else{
                            write(fd, "Table failed to remove\n", SIZE_BUFFER);
                        }
                    }
                }
            }
            else{
                write(fd, "Drop Failed\n", SIZE_BUFFER);
            }
        }
        else if(strcmp(command, "DELETE") == 0){
            command = strtok(NULL," ");

            if(strcmp(command, "FROM") == 0){
                char string[10000], cwd[PATH_MAX];
                char *token = strtok(NULL, " ");

                if(getcwd(cwd, sizeof(cwd)) != NULL){
                    FILE *data;
                    char input[1000];
                    char *token1 = strtok(token, ";");

                    if(strstr(cwd, "all_database/") == NULL){
                        write(fd, "Database not use!\n", SIZE_BUFFER);
                    }else{
                        data = fopen(string,"r");

                        if(data){
                            fgets(input, 100, data);
                            fclose(data);

                            data = fopen(string, "w");
                            fprintf(data, "%s\n", input);
                            write(fd, "Data deleted successfuly\n", SIZE_BUFFER);
                            fclose(data);
                        }else{
                            write(fd, "Data failed to delete\n", SIZE_BUFFER);
                        }
                    }

                }
            }
        }
        else if(strcmp(command, "INSERT") == 0){
            command = strtok(NULL, " ");

            if(strcmp(command, "INTO") == 0){
                char fileDir[10000], cwd[PATH_MAX];
                char *nama_tabel = strtok(NULL, " ");

                printf("Tabel: %s\n", nama_tabel);

                if(getcwd(cwd, sizeof(cwd)) != NULL){
                    FILE *data;
                    char *value = strtok(NULL, "()");

                    printf("Value: %s\n", value);
                    sprintf(fileDir, "%s/%s.txt", cwd, nama_tabel);

                    printf("Directory: %s\n", fileDir);

                    if(strstr(cwd, "all_database/") == NULL){
                        write(fd, "Database not use!\n", SIZE_BUFFER);
                    } else if(value == NULL){
                        write(fd, "Invald syntax!\n", SIZE_BUFFER);
                    } else if(checkFile(fileDir)){
                        data = fopen(fileDir, "a");

                        fprintf(data, "%s\n", value);
                        fclose(data);
                        write(fd, "Data input successfuly\n", SIZE_BUFFER);
                    } else{
                        write(fd, "Tabel not found!\n", SIZE_BUFFER);
                    }
                }
            }
        }
        else{
            write(fd,"Invalid query\n", SIZE_BUFFER);
        }
    }
    if(fd == curr_fd){
        curr_fd = curr_ids = -1;
    }
    close(fd);
}


int main(){
    pid_t pid,sid;
    pid = fork();

    if(pid < 0) exit(EXIT_FAILURE);
    if(pid > 0) exit(EXIT_SUCCESS);

    umask(0);

    sid = setsid();
    if(sid < 0) exit(EXIT_FAILURE);

    getcwd(workDir, sizeof(workDir));

    if((chdir(workDir)) < 0) exit(EXIT_FAILURE);

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    while(true){

        int server_fd, new_socket, valread, opt = 1;
        struct sockaddr_in address, new_address;
        socklen_t addrlen;
        pthread_t tid;
        char buffer[1000];

        server_fd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
        if (server_fd == -1) {
            fprintf(stderr, "Socket Failed [%s]\n", strerror(errno));
            exit(EXIT_FAILURE);
        }
      
        if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
            perror("setsockopt");
            exit(EXIT_FAILURE);
        }

        address.sin_family = AF_INET;
        address.sin_addr.s_addr = INADDR_ANY;
        address.sin_port = htons( PORT );
      
        if ((bind(server_fd, (struct sockaddr *)&address, sizeof(struct sockaddr_in))) != 0) {
            fprintf(stderr, "bind failed");
            close(server_fd);
            exit(EXIT_FAILURE);
        }

        if (listen(server_fd, 5) != 0) {
            fprintf(stderr,"listen failed");
            close(server_fd);
            exit(EXIT_FAILURE);
        }

        while(true){
            if ((new_socket = accept(server_fd, (struct sockaddr *)&new_address, (socklen_t*)&addrlen)) >= 0) {
                pthread_create(&tid, NULL, &routes, (void *)&new_socket);
            }
        }
    }
    return 0;
}